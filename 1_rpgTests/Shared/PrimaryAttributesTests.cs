using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests
{
    public class PrimaryAttributesTests
    {
        [Fact]
        public void Add_AddTwoPrimaryAttributes_ResultShouldBeSumOfTheTwo()
        {
            // Arrange
            var pa1 = new PrimaryAttributes(2, 3, 4);
            var pa2 = new PrimaryAttributes(8, 7, 6);
            var expected = new PrimaryAttributes(10, 10, 10);

            // Act
            var actual = pa1 + pa2;

            // Assert
            Assert.Equal(actual, expected);
        }
        [Fact]
        public void Equals_InitTwoIdenticalPrimaryAttributes_ShouldBeEqual()
        {
            // Arrange
            var pa1 = new PrimaryAttributes(1, 2, 3);
            var pa2 = new PrimaryAttributes(1, 2, 3);
            var expected = true;

            // act
            var actual = pa1 == pa2;

            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Equals_InitTwoDifferntPrimaryAttributes_ShouldNotBeEqual()
        {
            // Arrange
            var pa1 = new PrimaryAttributes(1, 2, 3);
            var pa2 = new PrimaryAttributes(7, 8, 9);
            var expected = false;

            // act
            var actual = pa1 == pa2;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}