using _1_rpg.Errors;
using _1_rpgTests.Helpers;
using Xunit;

namespace _1_rpgTests.ItemTests
{
    public class ArmorTests
    {
        [Theory]
        [InlineData(Characters.RANGER)]
        [InlineData(Characters.ROGUE)]
        [InlineData(Characters.WARRIOR)]
        public void EquipItem_IntializeCharactersThatCantEquipClothArmorAndEquipClothArmor_ShouldThrowInvalidArmorException(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var clothArmor = TestArmor.TestClothHead;
            
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => character.EquipItem(clothArmor));
        }

        [Theory]
        [InlineData(Characters.MAGE)]
        public void EquipItem_InitializeCharacterThatCanEquipClothArmorAndEquipClothArmor_ShouldReturnSuccessString(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var clothArmor = TestArmor.TestClothHead;
            var expected = "New armor equipped!";

            // Act
            var actual = character.EquipItem(clothArmor);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(Characters.MAGE)]
        public void EquipItem_IntializeCharactersThatCantEquipMailArmorAndEquipMailArmor_ShouldThrowInvalidArmorException(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var mailArmor= TestArmor.TestMailLegs;
            
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => character.EquipItem(mailArmor));
        }

        [Theory]
        [InlineData(Characters.RANGER)]
        [InlineData(Characters.ROGUE)]
        [InlineData(Characters.WARRIOR)]
        public void EquipItem_InitializeCharacterThatCanEquipMailArmorAndEquipMailArmor_ShouldReturnSuccessString(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var mailArmor = TestArmor.TestMailLegs;
            var expected = "New armor equipped!";

            // Act
            var actual = character.EquipItem(mailArmor);

            // Assert
            Assert.Equal(expected, actual);
        }
        //
        // ... do this for all armors ?
        //
    }
}