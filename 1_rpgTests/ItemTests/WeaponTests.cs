using _1_rpgTests.Helpers;
using _1_rpg.Errors;
using Xunit;

namespace _1_rpgTests.ItemTests
{
    public class WeaponTests
    {
        [Theory]
        [InlineData(Characters.MAGE)]
        [InlineData(Characters.RANGER)]
        [InlineData(Characters.ROGUE)]
        public void EquipItem_InitializeCharacterThatCantUseAxesAndEquipAxe_ShouldThrowInvaldWeaponException(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var axe = TestWeapon.TestAxe;

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => character.EquipItem(axe));
        }
        [Theory]
        [InlineData(Characters.WARRIOR)]
        public void EquipItem_InitializeCharacterThatCanUseAxesAndEquipAxe_ShouldReturnSuccessString(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var axe = TestWeapon.TestAxe;
            var expected = "New weapon equipped!";

            // Act
            var actual = character.EquipItem(axe);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(Characters.MAGE)]
        [InlineData(Characters.ROGUE)]
        [InlineData(Characters.WARRIOR)]
        public void EquipItem_InitializeCharacterThatCantUseBowsAndEquipBows_ShouldThrowInvaldWeaponException(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var bow = TestWeapon.TestBow;

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => character.EquipItem(bow));
        }
        [Theory]
        [InlineData(Characters.RANGER)]
        public void EquipItem_InitializeCharacterThatCanUseBowsAndEquipBow_ShouldReturnSuccessString(Characters characters)
        {
            // Arrange
            var character = ClassFactory.GetCharacter(characters);
            var bow = TestWeapon.TestBow;
            var expected = "New weapon equipped!";

            // Act
            var actual = character.EquipItem(bow);

            // Assert
            Assert.Equal(expected, actual);
        }
        //  
        // ... do this for all weapons maybe?
        // 
    }
}