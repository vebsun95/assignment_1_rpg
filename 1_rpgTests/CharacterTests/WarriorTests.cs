using _1_rpg.Character;
using _1_rpg.Errors;
using _1_rpg.Items;
using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests.CharacterTests
{
    public class WarriorTests
    {
        [Fact]
        public void TotalAttributes_InitializeWarrior_ShouldHave5Str2DexAnd1Int()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            var expected = new PrimaryAttributes(
                strength: 5,
                dexterity: 2,
                intelligence: 1
            );

            // Act
            var actual = warrior.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_InitializeWarrior_ShouldHave8Str4DexAnd2Int()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            warrior.LevelUp();
            var expected = new PrimaryAttributes(
                strength: 8,
                dexterity: 4,
                intelligence: 2
            );

            // Act
            var actual = warrior.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipWeapon_InitializeWarriorAndEquipBow_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            var warrior = new Warrior("Dummy");
            var bow     = new Weapon(
                itemName: "Common Bow",
                itemLevel: 1,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.BOW,
                new WeaponAttributes(
                    damage: 12,
                    attackSpeed: 0.8f
                )
            );

            // Act

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(bow));
        }
        [Fact]
        public void EquipArmor_IntializeWarriorAndEquipClothArmorType_ShouldThrowInvalidArmorException()
        {
            // Arrange
            var warrior = new Warrior("Dummy");
            var armor   = new Armor(
                itemName: "Common cloth head armor",
                itemLevel: 1, 
                itemSlot: ItemSlot.BODY,
                armorType: ArmorType.CLOTH,
                attributes: new PrimaryAttributes());
            
            // Act

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(armor));
        }
        [Fact]
        public void CharacterDamgage_LevelOneWarriorNoWeapon_ShouldHave1Point05Damage()
        {
            // Arrange
            var warrior = new Warrior("Dummy");
            var expected = 1.05f; // 1 * 1.05;

            // Act
            var actual = warrior.CharacterDamage;

            // Assert
            Assert.Equal(expected, actual, 2);
        }
        [Fact]
        public void CharacterDamage_LevelOneWarriorWithAxe_DamgeShouldBeEightPoint162()
        {
            // Arrange
            var warrior = new Warrior("Dummy");
            var axe     = new Weapon(
                itemName: "Common axe",
                itemLevel: 1,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.AXE,
                weaponAttributes: new WeaponAttributes(7, 1.1f)
            );
            warrior.EquipItem(axe);
            var expected = 8.085f; // (7 * 1.1) + (1 + (5 / 100))

            // Act
            var actual = warrior.CharacterDamage;

            // Assert
            Assert.Equal(expected, actual, 3);
        }
        [Fact]
        public void CharacterDamage_Level1WarriorWithAxeAndPlateBody_DamageShouldBe8Point162()
        {
            // Arrange
            var warrior = new Warrior("Dummy");
            var axe     = new Weapon(
                itemName: "Common axe",
                itemLevel: 1,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.AXE,
                weaponAttributes: new WeaponAttributes(7, 1.1f)
            );
            var armor   = new Armor(
                itemName: "Common plate body armor",
                itemLevel: 1, 
                itemSlot: ItemSlot.BODY,
                armorType: ArmorType.PLATE,
                attributes: new PrimaryAttributes( strength: 1)
            );
            warrior.EquipItem(axe);
            warrior.EquipItem(armor);
            var expected = 8.162f; // (7 * 1.1) + (1 + ((5 + 1) / 100))

            // Act
            var actual = warrior.CharacterDamage;

            // Assert
            Assert.Equal(expected, actual, 3);
        }
    }
}