using _1_rpg.Character;
using _1_rpg.Items;
using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests.CharacterTests
{
    public class MageTests
    {
       [Fact]
        public void TotalAttributes_InitializeMage_ShouldHave1Str1DexAnd8Intelligence()
        {
            //Arrange
            var mage = new Mage("Dummy");
            var expected = new PrimaryAttributes(
                strength: 1,
                dexterity: 1,
                intelligence: 8
            );

            // Act
            var actual = mage.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
       [Fact]
        public void TotalAttributes_InitializeMageAndLevelUp_ShouldHave2Str2DexAnd13Intelligence()
        {
            //Arrange
            var mage = new Mage("Dummy");
            mage.LevelUp();
            var expected = new PrimaryAttributes(
                strength: 2,
                dexterity: 2,
                intelligence: 13
            );

            // Act
            var actual = mage.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipWeapon_InitializeMageAndEquipLevel1Wand_ShouldReturnNewWeaponEquippedString()
        {
            // Arrange
            var mage = new Mage("Dummy");
            var wand = new Weapon(
                itemName: "Common wand",
                itemLevel: 1,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.WAND,
                weaponAttributes: new WeaponAttributes(1, 1.0f)
            );
            var expected = "New weapon equipped!";

            // Act
            var actual = mage.EquipItem(wand);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}