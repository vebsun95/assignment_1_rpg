using _1_rpg.Character;
using _1_rpg.Errors;
using _1_rpg.Items;
using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests.CharacterTests
{
    public class CharacterBaseTests
    {
        [Fact]
        public void ConstructorMage_Intialize_ShouldBeLevel1()
        {
            //Arrange
            var mage = new Mage("Dummy");
            var expected = 1;

            // Act
            var actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ConstructorRanger_Intialize_ShouldBeLevel1()
        {
            //Arrange
            var ranger = new Ranger("Dummy");
            var expected = 1;

            // Act
            var actual = ranger.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ConstructorRouge_Intialize_ShouldBeLevel1()
        {
            //Arrange
            var rogue = new Rogue("Dummy");
            var expected = 1;

            // Act
            var actual = rogue.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ConstructorWarrior_Intialize_ShouldBeLevel1()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            var expected = 1;

            // Act
            var actual = warrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_IntializeMageAndLevelUp_ShouldBeLevel2()
        {
            //Arrange
            var mage = new Mage("Dummy");
            mage.LevelUp();
            var expected = 2;

            // Act
            var actual = mage.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_IntializeRangerAndLevelUp_ShouldBeLevel2()
        {
            //Arrange
            var ranger = new Ranger("Dummy");
            ranger.LevelUp();
            var expected = 2;

            // Act
            var actual = ranger.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_IntializeRogueAndLevelUp_ShouldBeLevel2()
        {
            //Arrange
            var rogue = new Rogue("Dummy");
            rogue.LevelUp();
            var expected = 2;

            // Act
            var actual = rogue.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_IntializeWarriorAndLevelUp_ShouldBeLevel2()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            warrior.LevelUp();
            var expected = 2;

            // Act
            var actual = warrior.Level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipWeapon_IntializeWarriorAndEquipLevel2Axe_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            var axe     = new Weapon(
                itemName: "Common axe",
                itemLevel: 2,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.AXE,
                weaponAttributes: new WeaponAttributes(7, 1.1f));

            //Act

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(axe));
        }
        [Fact]
        public void EquipArmor_IntializeWarriorAndEquipLevel2PlateBodyArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            var warrior = new Warrior("Dummy");
            var armor   = new Armor(
                itemName: "Common plate body armor",
                itemLevel: 2, 
                itemSlot: ItemSlot.BODY,
                armorType: ArmorType.PLATE,
                attributes: new PrimaryAttributes()
            );

            //Act

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(armor));
        }
    }
}
