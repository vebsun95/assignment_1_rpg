using _1_rpg.Character;
using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests.CharacterTests
{
    public class RogueTests
    {
        [Fact]
        public void TotalAttributes_InitializeRouge_ShouldHave2Str6DexAnd1Int()
        {
            //Arrange
            var rogue = new Rogue("Dummy");
            var expected = new PrimaryAttributes(
                strength: 2,
                dexterity: 6,
                intelligence: 1
            );

            // Act
            var actual = rogue.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_InitializeRougeAndLevelUp_ShouldHave3Str10DexAnd2Int()
        {
            //Arrange
            var rogue = new Rogue("Dummy");
            rogue.LevelUp();
            var expected = new PrimaryAttributes(
                strength: 3,
                dexterity: 10,
                intelligence: 2
            );

            // Act
            var actual = rogue.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}