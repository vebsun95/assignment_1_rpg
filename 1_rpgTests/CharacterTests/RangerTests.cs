using _1_rpg.Character;
using _1_rpg.Items;
using _1_rpg.Shared;
using Xunit;

namespace _1_rpgTests.CharacterTests
{
    public class RangerTests
    {
        [Fact]
        public void TotalAttributes_InitializeRanger_ShouldHave1Str7DexAnd1Int()
        {
            //Arrange
            var ranger = new Ranger("Dummy");
            var expected = new PrimaryAttributes(
                strength: 1,
                dexterity: 7,
                intelligence: 1
            );

            // Act
            var actual = ranger.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void TotalAttributes_InitializeRangerAndLevelUp_ShouldHave2Str12DexAnd2Int()
        {
            //Arrange
            var ranger = new Ranger("Dummy");
            ranger.LevelUp();
            var expected = new PrimaryAttributes(
                strength: 2,
                dexterity: 12,
                intelligence: 2
            );

            // Act
            var actual = ranger.TotalAttributes;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipArmor_InitializeRangeAndEquipValidArmor_ShouldReturnNewArmourEquipped()
        {
            // Arrange
            var ranger = new Ranger("Dummy");
            var mailBody = new Armor(
                itemName: "Common mail body armor",
                itemLevel: 1,
                itemSlot: ItemSlot.BODY,
                armorType: ArmorType.MAIL,
                attributes: new PrimaryAttributes()
            );
            var expected = "New armor equipped!";

            // Act
            var actual = ranger.EquipItem(mailBody);

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}