using _1_rpg.Items;

namespace _1_rpgTests.Helpers
{
    public class TestWeapon
    {
        public static Weapon TestAxe => new Weapon
        (
            itemName: "Common Axe",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.AXE,
            weaponAttributes: new WeaponAttributes
                (
                    damage: 7,
                    attackSpeed: 1.1f
                )
        );
        public static Weapon TestBow => new Weapon
        (
            itemName: "Common Bow",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.BOW,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
        public static Weapon TestDagger => new Weapon
        (
            itemName: "Common Dagger",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.DAGGER,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
        public static Weapon TestHammer=> new Weapon
        (
            itemName: "Common Hammer",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.HAMMER,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
        public static Weapon TestStaff => new Weapon
        (
            itemName: "Common Staff",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.STAFF,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
        public static Weapon TestSword => new Weapon
        (
            itemName: "Common Sword",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.SWORD,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
        public static Weapon TestWand => new Weapon
        (
            itemName: "Common Wand",
            itemLevel: 1,
            itemSlot: ItemSlot.WEAPON,
            weaponType: WeaponType.WAND,
            weaponAttributes: new WeaponAttributes
            (
                damage: 12,
                attackSpeed: 0.8f
            )
        );
    }
}