using _1_rpg.Character;

namespace _1_rpgTests.Helpers
{
    public class ClassFactory
    {
        public static CharacterBase GetCharacter(Characters character)
        {
            switch (character)
            {
                case Characters.MAGE:
                    return new Mage("Dummy");
                case Characters.RANGER:
                    return new Ranger("Dummy");
                case Characters.ROGUE:
                    return new Rogue("Dummy");
                case Characters.WARRIOR:
                    return new Warrior("Dummy");
                default:
                    return new Mage("Dummy");
            }
        }
    }

    public enum Characters
    {
        MAGE,
        RANGER,
        ROGUE,
        WARRIOR
    }
}