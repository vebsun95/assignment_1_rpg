using _1_rpg.Items;
using _1_rpg.Shared;

namespace _1_rpgTests.Helpers
{
    public class TestArmor
    {
        public static Armor TestClothHead => new Armor
        (
            itemName: "Common cloth head",
            itemLevel: 1,
            itemSlot: ItemSlot.HEAD,
            armorType: ArmorType.CLOTH,
            attributes: new PrimaryAttributes
            (
                intelligence: 5
            )
        );
        public static Armor TestPlateBody => new Armor
        (
            itemName: "Common plate body armor",
            itemLevel: 1,
            itemSlot: ItemSlot.BODY,
            armorType: ArmorType.PLATE,
            attributes: new PrimaryAttributes
            (
                strength: 1
            )
        );
        public static Armor TestMailLegs => new Armor
        (
            itemName: "Common mail legs",
            itemLevel: 1,
            itemSlot: ItemSlot.LEGS,
            armorType: ArmorType.MAIL,
            attributes: new PrimaryAttributes
            (
                dexterity: 2
            )
        );
    }
}