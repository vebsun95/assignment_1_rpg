namespace _1_rpg.Items 
{
    public abstract class ItemBase
    {
        /// <summary>
        /// The items display name.
        /// </summary>
        public string ItemName { get; init; }
        /// <summary>
        /// A character must have equal of higher Level than ItemLevel inorder to equip it.
        /// </summary>
        public int ItemLevel { get; init; }
        /// <summary>
        /// Defined which equipement slot the item should occupate.
        /// </summary>
        public ItemSlot ItemSlot { get; init; }

        public ItemBase(string itemName, int itemLevel, ItemSlot itemSlot)
        {
            ItemName = itemName;
            ItemLevel = itemLevel;
            ItemSlot = itemSlot;
        }

    }

    public enum ItemSlot
    {
        WEAPON,
        BODY,
        HEAD,
        LEGS,
    }
}