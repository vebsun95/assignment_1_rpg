namespace _1_rpg.Items
{
    public class Weapon : ItemBase
    {
        public Weapon(string itemName, int itemLevel, ItemSlot itemSlot, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(itemName, itemLevel, itemSlot)
        {
            WeaponType = weaponType;
            WeaponAttributes = weaponAttributes;
        }

        public WeaponType WeaponType {get; init;} 
        public WeaponAttributes WeaponAttributes {get; init;}
        /// <summary>
        /// Use this property inorder to get the weapons DPS. 
        /// </summary>
        /// <value> Returns <c> WeaponAttributes.Damage * WeaponAttributes.AttackSpeed </c> </value>
        public float DPS
        {
            get
            {
                return WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
            }    
        }
    }
    public struct WeaponAttributes
    {
        public int Damage {get; set; }
        public float AttackSpeed {get; set; }
        public WeaponAttributes(int damage, float attackSpeed)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
    }
    public enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND,
    }

}