using _1_rpg.Shared;

namespace _1_rpg.Items
{
    public class Armor : ItemBase
    {
        public Armor(string itemName, int itemLevel, ItemSlot itemSlot, ArmorType armorType, PrimaryAttributes attributes) : base(itemName, itemLevel, itemSlot)
        {
            ArmorType = armorType;
            Attributes = attributes;
        }
        /// <summary>
        /// An example of armor type could be Cloth.
        /// </summary>
        public ArmorType ArmorType {get; init;}
        public PrimaryAttributes Attributes { get; init; } 
    } 
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE,
    }
}
