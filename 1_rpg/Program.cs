﻿using _1_rpg.Character;
using _1_rpg.Items;
using _1_rpg.Shared;

namespace _1_rpg
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var mage = new Mage("Merlin");
            var wand = new Weapon
            (
                itemName: "Sick wand",
                itemLevel: 4,
                itemSlot: ItemSlot.WEAPON,
                weaponType: WeaponType.WAND,
                weaponAttributes: new WeaponAttributes
                    (
                        damage: 69,
                        attackSpeed: 2
                    )
            );
            var hat = new Armor
            (
                itemName: "Mage hat",
                itemLevel: 3,
                itemSlot: ItemSlot.HEAD,
                armorType: ArmorType.CLOTH,
                attributes: new PrimaryAttributes
                    (
                        dexterity: 1,
                        intelligence: 9
                    )
            );
            var clothBody = new Armor
            (
                itemName: "Classic mage body armor",
                itemLevel: 3,
                itemSlot: ItemSlot.BODY,
                armorType: ArmorType.CLOTH,
                attributes: new PrimaryAttributes
                (
                    intelligence: 42
                )
            );

            for(int i=0; i<4; i++) 
            {
                mage.LevelUp();
            }
            mage.EquipItem(wand);
            mage.EquipItem(hat);
            mage.EquipItem(clothBody);
            System.Console.WriteLine(mage);
        }
    }
}
