namespace _1_rpg.Shared
{
    public struct PrimaryAttributes
    {
        public int Strength { get; private set; }
        public int Dexterity { get; private set; }
        public int Intelligence { get; private set; }

        public PrimaryAttributes(int strength = 0, int dexterity = 0, int intelligence = 0)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public static PrimaryAttributes operator +(PrimaryAttributes self, PrimaryAttributes other)
        {
            return new PrimaryAttributes(
                strength: self.Strength + other.Strength,
                dexterity: self.Dexterity + other.Dexterity,
                intelligence: self.Intelligence + other.Intelligence
            );
        }

        // PrimaryAttributes are considered equal if all attributes in both intstances are the same.
        public static bool operator ==(PrimaryAttributes self, PrimaryAttributes other)
        {
            return self.Strength == other.Strength && self.Dexterity == other.Dexterity && self.Intelligence == other.Intelligence;
        }
        public static bool operator !=(PrimaryAttributes self, PrimaryAttributes other)
        {
            return self.Strength != other.Strength && self.Dexterity != other.Dexterity && self.Intelligence != other.Intelligence;
        }
        public override bool Equals(object obj)
        {
            return base.Equals (obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}