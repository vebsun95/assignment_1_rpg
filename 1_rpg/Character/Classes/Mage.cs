using System.Linq;
using _1_rpg.Errors;
using _1_rpg.Items;
using _1_rpg.Shared;

namespace _1_rpg.Character
{
    /// <summary>
    /// The mage use intelligence as it primary stat, can wield staffs or wands and equip cloth as amor.
    /// </summary>
    public class Mage : CharacterBase
    {
        public Mage(string name) : base(name) {}
        public override PrimaryAttributes BaseAttributes => BASE_ATTRIBUTES;
        public override PrimaryAttributes LevelUpAttributes => LEVELUP_ATTRIBUTES;
        public override float CharacterDamage
        {
            get
            {
                float dps = 1;
                if(EquippedItems.ContainsKey(ItemSlot.WEAPON))
                   dps = ((Weapon) EquippedItems[ItemSlot.WEAPON]).DPS;
                // A mages primary attribute is interlligence
                return dps * (1.0f + (TotalAttributes.Intelligence / 100.0f));
            }
        }
        public override string EquipItem(Armor armor)
        {
            if( !ALLOWED_ARMOR_TYPES.Contains(armor.ArmorType))
                throw new InvalidArmorException();
            return base.EquipItem(armor);
        }
        public override string EquipItem(Weapon weapon)
        {
            if( !ALLOWED_WEAPON_TYPES.Contains(weapon.WeaponType))
                throw new InvalidWeaponException();
            return base.EquipItem(weapon);
        }
        private static readonly PrimaryAttributes BASE_ATTRIBUTES = new PrimaryAttributes(1, 1, 8);
        private static readonly PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(1, 1, 5);
        private static readonly ArmorType[] ALLOWED_ARMOR_TYPES = new ArmorType[] {ArmorType.CLOTH}; 
        private static readonly WeaponType[] ALLOWED_WEAPON_TYPES = new WeaponType[] {WeaponType.STAFF, WeaponType.WAND};
    }
}