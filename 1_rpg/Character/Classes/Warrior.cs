using System.Linq;
using _1_rpg.Errors;
using _1_rpg.Items;
using _1_rpg.Shared;

namespace _1_rpg.Character
{
    /// <summary>
    /// The warrior uses strength as it primary attribute, can equip both mail and plate armor and wield an axe, hammer or sword. 
    /// </summary>
    public class Warrior : CharacterBase
    {
        public Warrior(string name) : base(name) {}
        public override PrimaryAttributes BaseAttributes => BASE_ATTRIBUTES;
        public override PrimaryAttributes LevelUpAttributes => LEVELUP_ATTRIBUTES;
        public override float CharacterDamage
        {
            get
            {
                float dps = 1;
                if(EquippedItems.ContainsKey(ItemSlot.WEAPON))
                   dps = ((Weapon) EquippedItems[ItemSlot.WEAPON]).DPS;
                // A warriors main attribute is strength.
                return dps * (1.0f + (TotalAttributes.Strength / 100.0f));
            }
        }
        public override string EquipItem(Armor armor)
        {
            if( !ALLOWED_ARMOR_TYPES.Contains(armor.ArmorType))
                throw new InvalidArmorException();
            return base.EquipItem(armor);
        }
        public override string EquipItem(Weapon weapon)
        {
            if( !ALLOWED_WEAPON_TYPES.Contains(weapon.WeaponType))
                throw new InvalidWeaponException();
            return base.EquipItem(weapon);
        }
        private static readonly PrimaryAttributes BASE_ATTRIBUTES = new PrimaryAttributes(5, 2, 1);
        private static readonly PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(3, 2, 1);
        private static readonly ArmorType[] ALLOWED_ARMOR_TYPES = new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE };
        private static readonly WeaponType[] ALLOWED_WEAPON_TYPES = new WeaponType[] { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD};
    }
}