using System.Linq;
using _1_rpg.Errors;
using _1_rpg.Items;
using _1_rpg.Shared;

namespace _1_rpg.Character
{
    /// <summary>
    /// The ranger uses dexterity as it primary attribute, can wield dagger or sword and use leather or mail as armor
    /// </summary>
    public class Ranger : CharacterBase
    {
        public Ranger(string name) : base(name) {}
        public override PrimaryAttributes BaseAttributes => BASE_ATTRIBUTES;
        public override PrimaryAttributes LevelUpAttributes => LEVELUP_ATTRIBUTES;
        public override float CharacterDamage
        {
            get
            {
                float dps = 1;
                if(EquippedItems.ContainsKey(ItemSlot.WEAPON))
                   dps = ((Weapon) EquippedItems[ItemSlot.WEAPON]).DPS;
                // A Rangers main attribute is Dexterity.
                return dps * (1.0f + (TotalAttributes.Dexterity / 100.0f));
            }
        }
        public override string EquipItem(Armor armor)
        {
            if( !ALLOWED_ARMOR_TYPES.Contains(armor.ArmorType))
                throw new InvalidArmorException();
            return base.EquipItem(armor);
        }
        public override string EquipItem(Weapon weapon)
        {
            if( !ALLOWED_WEAPON_TYPES.Contains(weapon.WeaponType))
                throw new InvalidWeaponException();
            return base.EquipItem(weapon);
        }
        private static readonly PrimaryAttributes BASE_ATTRIBUTES = new PrimaryAttributes(1, 7, 1);
        private static readonly PrimaryAttributes LEVELUP_ATTRIBUTES = new PrimaryAttributes(1, 5, 1);
        private static readonly ArmorType[] ALLOWED_ARMOR_TYPES = new ArmorType[] {ArmorType.LEATHER, ArmorType.MAIL};
        private static readonly WeaponType[] ALLOWED_WEAPON_TYPES = new WeaponType[] { WeaponType.BOW };
    }
}