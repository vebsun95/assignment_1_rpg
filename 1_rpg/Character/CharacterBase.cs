using System.Linq;
using _1_rpg.Items;
using _1_rpg.Errors;
using System.Collections.Generic;
using _1_rpg.Shared;

namespace _1_rpg.Character
{
    public abstract class CharacterBase
    {
        // Fields

        /// <summary>
        /// Holds the characters display name.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// The characters current level. Use the LevelUp method inorder to increase. 
        /// </summary>
        public int Level { get; private set; } = 1;

        /// <summary>
        /// Returns a dicitionary containing the characters currently equipped items.
        /// To equip an item use the method EquipItem(ItemBase item); 
        /// </summary>
        /// <returns>A dictionary containing currently equipped items</returns>
        public Dictionary<ItemSlot, ItemBase> EquippedItems { get; init; } = new();
        public CharacterBase(string name)
        {
            Name = name;
        }

        // Properties

        /// <summary>
        /// Calculates the characters total attributes from base, level and equipment stats.
        /// </summary>
        /// <value> The characters totalt attribute score </value>
        public PrimaryAttributes TotalAttributes
        {
            get
            {
                // Start of with the characters base attributes
                PrimaryAttributes totalAttribute = BaseAttributes;
                // Add attributes gained from levels
                for(var i=1; i < Level; i++)
                    totalAttribute += LevelUpAttributes;
                // Iterate over all equipped items where the item is not null && is of type Armor
                foreach(Armor armor in EquippedItems.Values
                    .Where(a => a.GetType() == typeof(Armor)))
                    // Add attributes from currently equiped armor.
                    totalAttribute += armor.Attributes;
                return totalAttribute;
            }
        }

        // Methods

        /// <summary>
        /// Equips the given weapon on the character. 
        /// </summary>
        /// <param name="weapon"> weapon to be equipped </param>
        /// <returns> A success string</returns>
        /// <exception cref="InvalidWeaponException">
        /// When the character cant wield this type of weapon or the weapon level is higher than the current character level
        /// </exception>
        public virtual string EquipItem(Weapon weapon)
        {
            if(Level < weapon.ItemLevel)
                throw new InvalidWeaponException();
            EquippedItems[weapon.ItemSlot] = weapon; 
            return "New weapon equipped!";
        }

        /// <summary>
        /// Equips the given armor on the character. 
        /// </summary>
        /// <param name="armor"> Armor to be equipped </param>
        /// <returns> A success string</returns>
        /// <exception cref="InvalidArmorException">
        /// When the character cant equiped this type of armor or the armor level is higher than the current character level.
        /// </exception>
        public virtual string EquipItem(Armor armor)
        {
            if(Level < armor.ItemLevel)
                throw new InvalidArmorException();
            EquippedItems[armor.ItemSlot] = armor;
            return "New armor equipped!";
        }
        /// <summary>
        /// Increments the current level of the character.
        /// </summary>
        /// <returns> void </returns>
        public void LevelUp()
        {
            Level++;
        }
        public override string ToString()
        {
            var totalAttribute = TotalAttributes;
            return 
            @$"
Character name  : {Name}
Class           : {this.GetType().Name}
Character level : {Level}
Strength        : {totalAttribute.Strength}
Dexterity       : {totalAttribute.Dexterity}
Intelligence    : {totalAttribute.Intelligence}
Damage          : {CharacterDamage}";
        }

        // Abstract members

        // Note: Would be preferable if theese field where static, but i don't think thats possible? 

        /// <summary>
        /// Returns the characters starting attributes aka. base attributes 
        /// </summary>
        /// <value> starting- / base attributes </value>
        public abstract PrimaryAttributes BaseAttributes { get; }

        /// <summary>
        /// Returns the characters Level up attributes.
        /// When a character gains a level their totalt attributes increases with their 
        /// Level Up attributes.
        /// </summary>
        /// <value>The charcters level up attributes </value>
        public abstract PrimaryAttributes LevelUpAttributes { get; }
        
        /// <summary>
        /// Calcualtes the characters current damages from it's weapon and total primary attribute.
        /// If the character is currently unarmed the Damages is calculated using a weapon DPS = 1. 
        /// </summary>
        /// <value>Returns the charaters current damage output</value>
        public abstract float CharacterDamage { get; }
    } 

}